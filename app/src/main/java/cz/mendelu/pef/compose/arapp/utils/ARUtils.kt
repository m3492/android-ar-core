package cz.mendelu.pef.compose.arapp.utils

import com.google.ar.core.Pose
import com.google.ar.sceneform.math.Vector3
import kotlin.math.sqrt

object ARUtils {

    fun getDistanceToObject(object1Pose: Pose, object2Pose: Pose): Float {
        val distanceX = object1Pose.tx() - object2Pose.tx()
        val distanceY = object1Pose.ty() - object2Pose.ty()
        val distanceZ = object1Pose.tz() - object2Pose.tz()
        return sqrt((distanceX * distanceX + distanceY * distanceY + distanceZ * distanceZ).toDouble())
            .toFloat()
    }

    fun getDistanceToObject(object1Position: Vector3, object2Position: Vector3): Float {
        val distanceX = object1Position.x - object2Position.x
        val distanceY = object1Position.y - object2Position.y
        val distanceZ = object1Position.z - object2Position.z
        return sqrt((distanceX * distanceX + distanceY * distanceY + distanceZ * distanceZ).toDouble())
            .toFloat()
    }
}