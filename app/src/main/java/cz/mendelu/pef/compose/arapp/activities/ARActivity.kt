package cz.mendelu.pef.compose.arapp.activities

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import com.google.ar.core.*
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.FrameTime
import com.google.ar.sceneform.Node
import com.google.ar.sceneform.Scene
import com.google.ar.sceneform.collision.Ray
import com.google.ar.sceneform.math.Vector3
import com.google.ar.sceneform.math.Vector3Evaluator
import com.google.ar.sceneform.rendering.*
import com.google.ar.sceneform.ux.BaseArFragment
import com.google.ar.sceneform.ux.TransformableNode
import com.gorisse.thomas.sceneform.scene.await
import cz.mendelu.pef.compose.arapp.R
import cz.mendelu.pef.compose.arapp.ar.ARMainFragment
import cz.mendelu.pef.compose.arapp.databinding.ActivityAractivityBinding
import cz.mendelu.pef.compose.arapp.utils.ARUtils


class ARActivity : AppCompatActivity(), BaseArFragment.OnTapArPlaneListener,
    Scene.OnUpdateListener {

    private lateinit var binding: ActivityAractivityBinding

    private lateinit var arFragment: ARMainFragment

    private var targetHitModel: Renderable? = null
    private var sphereModel: Renderable? = null

    private var mainObjectAnchor: Anchor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAractivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        arFragment = ARMainFragment()
        supportFragmentManager.commit {
            add(R.id.containerFragment, arFragment)
        }
        arFragment.setOnTapPlaneListener(this)
        arFragment.setOnSceneChangeListener(this)

        lifecycleScope.launchWhenCreated {
            // init renderable models
            loadTargetHitModel("models/halloween.glb")
            createSphereModel()
        }

    }

    private suspend fun loadTargetHitModel(path: String) {
        targetHitModel = ModelRenderable.builder()
            .setSource(this, Uri.parse(path))
            .setIsFilamentGltf(true)
            .await()
    }

    private fun createSphereModel() {
        MaterialFactory.makeTransparentWithColor(applicationContext, Color(255f, 0f, 0f))
            .thenAccept {
                val sphere = ShapeFactory.makeSphere(0.01f, Vector3(0f, 0f, 0f), it)
                sphere.isShadowCaster = false
                sphere.isShadowReceiver = false
                sphereModel = sphere
            }
    }

    override fun onTapPlane(hitResult: HitResult?, plane: Plane?, motionEvent: MotionEvent?) {
        if (targetHitModel == null) {
            Toast.makeText(this, "Model not loaded...", Toast.LENGTH_SHORT).show()
            return
        }

        val node = createChildNode(hitResult, true, targetHitModel!!)
        node?.let {
            arFragment.addModel(it)
        }
    }


    private fun createChildNode(
        hitResult: HitResult?,
        animate: Boolean,
        modelRenderable: Renderable
    ): Node? {
        hitResult?.let {
            mainObjectAnchor = hitResult.createAnchor()
            val anchorNode = AnchorNode(mainObjectAnchor)
            val transformableNode = TransformableNode(arFragment.getTransformationSystem())
            transformableNode.renderable = modelRenderable
            transformableNode.renderableInstance.setCulling(false)
            transformableNode.renderableInstance.animate(animate)
            transformableNode.localScale = Vector3(0.5f, 0.5f, 0.5f)
            transformableNode.select()
            anchorNode.addChild(transformableNode)
            return anchorNode
        }
        return null
    }

    override fun onUpdate(frameTime: FrameTime?) {
        val frame: Frame? = arFragment.getFrame()
        frame?.let {
            val camera = frame.camera
            if (camera.trackingState == TrackingState.TRACKING && mainObjectAnchor != null) {
                val cameraPose = camera.displayOrientedPose
                val distanceInMeters: Float =
                    ARUtils.getDistanceToObject(mainObjectAnchor!!.getPose(), cameraPose)
                Log.i("AAAA", "Distance: " + distanceInMeters)
            }
        }
    }

    fun shoot(view: View) {
        val sphereNode = createSphere()
        if (sphereNode == null) {
            Log.w("SPHERE_NOT_PRESENT", "Sphere can't be shooted yet!")
            return
        }

        val selectedNode = arFragment.getTransformationSystem().selectedNode
        if (selectedNode == null) {
            Log.w("SELECTED_NODE_NOT_PRESENT", "Sphere can't be shooted!")
            return
        }

        animateSphereHitsTarget(sphereNode, selectedNode, 1500)
    }

    private fun shootSphereCameraForward(sphere: TransformableNode) {

        val scene = arFragment.getSceneViewScene()
        val sceneCamera = scene.camera
        val ray = Ray(sceneCamera.worldPosition, sceneCamera.forward)

        Thread {
            for (i in 0..400) {
                runOnUiThread {
                    sphere.worldPosition = Vector3(ray.getPoint(i * 0.05f))
                }

                try {
                    Thread.sleep(20)
                } catch (ex: InterruptedException) {
                    ex.printStackTrace()
                }
            }
        }.start()
    }

    private fun animateSphereHitsTarget(
        sphereNode: TransformableNode,
        targetNode: Node,
        durationInMs: Long
    ) {
        val frame = arFragment.getFrame()
        if (frame != null) {
            val objectAnimator = ObjectAnimator()
            objectAnimator.setAutoCancel(true)
            // target = node that will be animated (sphere)
            objectAnimator.target = sphereNode
            objectAnimator.setObjectValues(getCameraPosition(), targetNode.worldPosition)
            objectAnimator.setPropertyName("worldPosition")
            objectAnimator.setEvaluator(Vector3Evaluator())
            objectAnimator.interpolator = LinearInterpolator()
            objectAnimator.duration = durationInMs
            objectAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    // remove target (placed) node using its parent - anchor node
                    targetNode.parentNode?.let { arFragment.removeNodeFromScene(it) }
                    // remove shooted sphere using its anchor node
                    sphereNode.parentNode?.let { arFragment.removeNodeFromScene(it) }
                    // select lastly placed node as next shooting target
                    if (arFragment.getSceneViewScene().children.isNotEmpty()) {
                        val lastSceneNode = arFragment.getSceneViewScene().children.last()
                        if (lastSceneNode.children.isNotEmpty()) {
                            val possibleTargetNode = lastSceneNode.children[0]
                            if (possibleTargetNode is TransformableNode) {
                                possibleTargetNode.select()
                            }
                        }
                    }
                }
            })
            objectAnimator.start()
        }
    }

    private fun createSphere(): TransformableNode? {

        val frame = arFragment.getFrame()
        val sphereAnchorNode = createSphereAnchorNode()

        if (frame != null && sphereAnchorNode != null && sphereModel != null) {
            val transformableNode = TransformableNode(arFragment.getTransformationSystem())
            transformableNode.renderable = sphereModel
            transformableNode.parent = sphereAnchorNode
            sphereAnchorNode.addChild(transformableNode)
            arFragment.addModel(sphereAnchorNode)
            transformableNode.worldPosition = getCameraPosition()
            return transformableNode
        }
        return null
    }


    private fun createSphereAnchorNode(): AnchorNode? {
        val session = arFragment.getSceneViewSession()
        val frame = arFragment.getFrame()
        if (session != null && frame != null && frame.camera.trackingState == TrackingState.TRACKING) {
            val cameraPosition = getCameraPositionAsFloatArray()
            val cameraRotation = floatArrayOf(0f, 0f, 0f, 0f)
            val sphereAnchor = session.createAnchor(Pose(cameraPosition, cameraRotation))
            val sphereAnchorNode = AnchorNode(sphereAnchor)
            sphereAnchorNode.parent = arFragment.getSceneViewScene()
            return sphereAnchorNode
        }
        return null
    }

    private fun getCameraPositionAsFloatArray(): FloatArray {
        val cameraPose = getCameraPose()
        return floatArrayOf(cameraPose.tx(), cameraPose.ty(), cameraPose.tz())
    }

    private fun getCameraPosition(): Vector3 {
        val cameraPose = getCameraPose()
        return Vector3(cameraPose.tx(), cameraPose.ty(), cameraPose.tz())
    }

    private fun getCameraPose(): Pose {
        val frame = arFragment.getFrame() ?: throw RuntimeException("Frame is not present!")
        val camera = frame.camera
        return camera.displayOrientedPose
    }
}